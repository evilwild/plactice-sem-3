import java.util.Arrays;

/*
 * Основной класс программы.
 * флаг -pi вызывает программную обработку ввода с консоли
 * флаг -t передает строку в программу
 * флаг -f передает путь к файлу в программу
 */
public class Program {

    public static void main(String[] args) throws Exception {
    	String input = null;
    	
    	if (args.length == 0) {
    		throw new Exception("No commandline arguments given");
    	} 
    	
    	switch (args[0]) {
		case "-pi":
			System.out.println("You've chosen program input mode");
			InputInterface console = new ConsoleInput();
			input = console.getInput();
			break;
			
		case "-t":
			if (args.length > 4) {
	    		throw new Exception("Too many arguments given");
	    	}
			System.out.println("You've chosen commandline input mode");
			input = args[1];
			break;
			
		case "-f":
			if (args.length > 4) {
	    		throw new Exception("Too many arguments given");
	    	}
			System.out.println("You've chosen file input mode");
			InputInterface fileInput = new FileInput(args[1]);
			input = fileInput.getInput();
			break;
		case "-h":
			System.out.printf("Available flags:\n\"-pi\" - program input method\n"
					+ "\"-t\" - takes text from next argument to program\n"
					+ "\"-f\" - takes path from next argument to program\n"
					+ "\"--to-file\" - saves output to given path\n"
					+ "\"--to-console\" - outputs text to console");
			break;
		default:
			throw new Exception("Invalid commandline argument given");
		}
    	
    	Code code = new Code(input);
    	code.Encode();
    	System.out.println(code.output);
    	code.Decode();
    	
    	if (Arrays.asList(args).contains("--to-console")) {
    		System.out.println(code.output);    		
    	} else if (Arrays.asList(args).contains("--to-file")) {
    		FileOutput fileOutput = new FileOutput(args[3]);
    		fileOutput.makeOutput(code.output);
    		System.out.println("Output written to file " + args[3]);
    	}
    	
    }
}
