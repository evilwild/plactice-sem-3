import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutput {
	String path;
	
	
	public FileOutput(String _path) {
		path = _path;
	}

	public void makeOutput(String input) {
		
		File file = null;
		FileOutputStream fileOutputStream = null;
		
		try {
			file = new File(path);
			fileOutputStream = new FileOutputStream(file);
			byte[] stringToBytes = input.getBytes();
			fileOutputStream.write(stringToBytes);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (IOException ex) {
					System.out.println(ex.getMessage());
				}
			}
		}
		
	}

}
