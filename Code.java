import java.util.ArrayList;
/*
 * Класс для кодирования/декодирования кода Хаффмана
*/
public class Code {
	public String input;
	public String output;
	private Tree tree = new Tree();
	private ArrayList<Boolean> encodedBits = new ArrayList<>();
	
	public Code(String source) {
		this.input = source;
		tree.Build(source);
	}
	
 	public void Encode() {
		ArrayList<Boolean> encodedSource = new ArrayList<>();
		
		for (int i = 0; i < input.length(); i++) {
			ArrayList<Boolean> encodedSymbol = tree.root.Traverse(input.charAt(i), new ArrayList<Boolean>());
			encodedSource.addAll(encodedSymbol);
		}
		encodedBits.addAll(encodedSource);
		
		String encodedString = "";
		
		for (Boolean bit : encodedSource) {
			char current = (bit) ? '1' : '0';
			encodedString += current;
		}
		
		output = encodedString;
	}
	
	public void Decode() {
		HuffmanNode current = tree.root;
		String decoded = "";
		
		for (Boolean bit : encodedBits) {
			if (bit) {
				if (!current.right.equals(null)) {
					current = current.right;
				}
			} else {
				if (!current.left.equals(null)) {
					current = current.left;
				}
			}
				if (tree.IsLeaf(current)) {
					decoded += current.symbol;
					current = tree.root;
				}
			
		}
		output = decoded;
	}
}