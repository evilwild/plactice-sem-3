import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
/*
 * Класс для дерева Хаффмана.
 */
public class Tree {

    public ArrayList<HuffmanNode> nodes = new ArrayList<>();
    public HuffmanNode root;
    public HashMap<Character, Integer> Frequencies = new HashMap<>();

    public void Build (String source) {

    	if (source.length() == 0) {
    		try {
				throw new Exception("Source string is 0-length");
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
    	}
    	
      for (int i = 0 ; i < source.length() ; i++) {
        char key = source.charAt(i);
        if (!Frequencies.containsKey(key)) {
          Frequencies.put(key, 0);
        }
        Frequencies.put(key, Frequencies.get(key)+1);
      }
   
      Frequencies.forEach((k, v) -> {
          nodes.add(new HuffmanNode(k, v));
      });
      
      while (nodes.size() > 1) {
		Collections.sort(nodes, new Comparator<HuffmanNode>() {
			@Override
			public int compare(HuffmanNode node1, HuffmanNode node2) {
				return node1.compareTo(node2);
			}
		});
		
		if (nodes.size() >= 2) {
			ArrayList<HuffmanNode> nodePairList = new ArrayList<HuffmanNode>(nodes.subList(0, 2));
			nodes.remove(1);
			nodes.remove(0);
			
			HuffmanNode left = nodePairList.get(1);
			HuffmanNode right = nodePairList.get(0);
			HuffmanNode parent = new HuffmanNode('*', left.frequency + right.frequency, left, right);
			nodes.add(parent);
		}
		
		this.root = nodes.get(nodes.size()-1);
      }
    }
    
    public Boolean IsLeaf(HuffmanNode node) {
    	return ((node.left == null) && (node.right == null));
    }
}
