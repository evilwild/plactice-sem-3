import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ConsoleInput implements InputInterface{

	@Override
	public String getInput() {
		String output = null;
		InputStream inputStream = null;
		BufferedReader bufferedReader = null;
		
		try {
			inputStream = System.in;
			bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			String input = null;
			
			System.out.printf("Type your text that you want to encode: ");
			
			 if ((input = bufferedReader.readLine()) != null) {
				
				System.out.printf("You're entered \"%s\"\n", input);
				output = input;
			 }
		} catch (Exception e) {
			System.out.println("Error while reading input stream");
		} finally {
			try {
				if (bufferedReader != null) {
					bufferedReader.close();
					System.out.println("Input stream successfully closed");
				}
			} catch (IOException ex) {
				System.out.println("Can't close input stream!");
			}
		}
		
		return output;
	}
	
}