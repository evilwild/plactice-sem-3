import java.util.ArrayList;
/*
* Базовый класс узла для дерева Хаффмана
*/
public class HuffmanNode implements Comparable<HuffmanNode> {
    public char symbol;
    public int frequency;
    public HuffmanNode left;
    public HuffmanNode right;
    
    public HuffmanNode(char _symbol, int _frequency) {
    	this.symbol = _symbol;
    	this.frequency = _frequency;
    }

    public HuffmanNode(char _symbol, int _frequency, HuffmanNode _left, HuffmanNode _right) {
		this.symbol = _symbol;
		this.frequency = _frequency;
		this.left = _left;
		this.right = _right;
	}

	public ArrayList<Boolean> Traverse(char symbol, ArrayList<Boolean> data) {
        if (this.right == null && this.left == null)
			{
				if (symbol == this.symbol)
				{
					return data;
				}
				else
				{
					return null;
				}
			}
			else
			{
				ArrayList<Boolean> left = null;
				ArrayList<Boolean> right = null;

				if (this.left != null)
				{
					ArrayList<Boolean> leftPath = new ArrayList<Boolean>();
                    leftPath.addAll(data);
					leftPath.add(false);

					left = this.left.Traverse(symbol, leftPath);
				}

				if (this.right != null)
				{
					ArrayList<Boolean> rightPath = new ArrayList<Boolean>();
					rightPath.addAll(data);
					rightPath.add(true);
					right = this.right.Traverse(symbol, rightPath);
				}

				if (left != null)
				{
					return left;
				}
				else
				{
					return right;
				}
			}
    }

	@Override
	public int compareTo(HuffmanNode node) {
		return this.frequency > node.frequency ? 1 : this.frequency < node.frequency ? -1 : 0;
	}

}
